const express = require('express')
const app = express()
const port = 3000
const fs = require('fs');
const path = require('path');

const convertHexToRGBA = (hexCode, opacity = 1) => {  
    let hex = hexCode.replace('#', '');
    
    if (hex.length === 3) {
        hex = `${hex[0]}${hex[0]}${hex[1]}${hex[1]}${hex[2]}${hex[2]}`;
    }    
    
    const r = parseInt(hex.substring(0, 2), 16);
    const g = parseInt(hex.substring(2, 4), 16);
    const b = parseInt(hex.substring(4, 6), 16);
    
    /* Backward compatibility for whole number based opacity values. */
    if (opacity > 1 && opacity <= 100) {
        opacity = opacity / 100;   
    }

    return `rgba(${r},${g},${b},${opacity})`;
};

app.get('/', (req, res) => {
    res.end('Hello World!');
});

app.get("/api/:hex", (req, res) => {
    // console.log(req.path.split('/')[2]);
    const rgba = convertHexToRGBA(req.params.hex);
    console.log(rgba)
    console.log(req.params.hex)
    const rgbaColor = {
        color : rgba
    }
    
    fs.writeFile(__dirname + '/' + 'color.json', JSON.stringify(rgbaColor, null, 2), err => {
        if (err) {
            console.log('Error writing file', err)
        } else {
            console.log('Successfully wrote file')
        }
    })

    fs.readFile(__dirname + '/' + 'color.json', 'utf8', (err, data) => {
        if (err){
            console.log("File read failed : ", err);
            return
        }
        res.end(
            data
        );
    });
});

app.listen(port, () => {
    console.log(`app listening at http://localhost:${port}`)
});